var whatIs = (val) => {
  if ( val === undefined )
    return 'undefined';

  if ( val === null )
    return 'null';

  switch (typeof val) {
    case 'number':
      return 'number';
    
    case 'string':
      return 'string';

    case 'boolean':
      return 'boolean';

    case 'function':
      return 'function';

    default:
      if ( val instanceof Array )
        return 'array';
      else
        return 'object';
  }
};

var clone = (value) => {
  switch (whatIs(value)) {
    case 'undefined':
      return undefined;

    case 'null':
      return null;

    case 'number':
    case 'boolean':
    case 'string':
    case 'function':
      return value;

    case 'array':
      return value.map( (x) => clone(x) );

    case 'object':
      let copy = {};
      for ( let i in value )
        if ( value.hasOwnProperty(i) )
          copy[i] = clone( value[i] );
      return copy;

    default:
      throw new Error('Unknown type');
  }
};

var intersection = (arr1, arr2) => {
  return arr1.filter( (elm) => arr2.indexOf(elm) > -1 );
};

var uniq = (arr) => {
  return arr.reduce( (acc, e) => {
    if (acc.indexOf(e) === -1)
      acc.push(e);
    return acc;
  }, [] );
};

var any = (arr, pred) => {
  return arr.reduce( (acc, e) => {
    if ( acc === true )
      return true;
    return pred(e);
  }, false);
};

var all = (arr, pred) => {
  return arr.reduce( (acc, e) => {
    if ( acc === false )
      return false;
    return pred(e);
  }, true);
};

var find = (arr, pred) => {
  return arr.filter( (e) => pred(e) )[0];
};

var min = (arr, name) => {
  return arr.reduce( (acc, elm) => {
    return (elm[name] < acc) ? elm[name] : acc;
  }, arr[0]);
};

var partition = (arr, pred) => {
  return arr.reduce( (acc, e) => {
    let [arr_t, arr_f] = acc;
    if ( pred(e) === true )
      arr_t.push(e);
    else
      arr_f.push(e);
    return [arr_t, arr_f];
  }, [[], []]);
};

var merge = (target, source) => {
  for ( let prop in source ) {
    if ( target.hasOwnProperty(prop) ) {
      switch (whatIs(target[prop])) {
        case 'object':
          merge(target[prop], source[prop]);
          break;
        default:
          target[prop] = clone(source[prop]);
      }
    }
    else
      target[prop] = clone( source[prop] );
  }
};

// The main, object-creating function.
var Net = function( conf = {} ) {
  return (function() {

    // the similation time
    var time;

    // the global token
    // and the timed changes information for it
    // I'm not sure, whether it's needed at all.
    var token;
    var global_time;

    // the arrays of places, arcs and transitions
    // Initially, all of them are simply copies of their specification
    // objects.
    var places;
    var arcs;
    var transitions;

    // a placeholder for error message
    // The error handling really needs refactoring (and rethinking).
    // Right now, it's messed up.
    var error;

    // objects used to handle net events
    // events - the container for all events.
    //   events.place :: array
    //   events.transition :: array
    // event_id - the next event (unique) identifier to use
    // events_handled - the flag set to true, when the events are
    // being handled
    //
    // Probably all the events stuff should me moved to a separate
    // module.
    var events;
    var event_id;
    var events_handled;

    // --------------------------------------------------------------
    // private functions
    // --------------------------------------------------------------

    // returns a deep clone of the argument
    var copyThat = (arr) => clone( arr );

    // This function, passed two arguments: the original value and the
    // default one, returns a copy (deep clone) of the original one
    // (if given), or the default one, when the original is not
    // specified.
    // It is used for initialisation of the net based on the given
    // specification.
    var getDefault = (orig, def) => {
      return ( orig !== undefined ) ? copyThat(orig) : def;
    };

    // This function should analyse the net.
    // Right now, it performs some basic checks (like unlinked places
    // or transitions).
    // It should be refactored and made more useful.
    var checkNet = () => {
      var places_of_arcs = arcs.map( (arc) => arc.place );
      var names_of_places = places.map( (p) => p.name );
      var common_places = intersection( places_of_arcs, names_of_places );
      // a place without an arc
      if ( places.length > common_places.length ) {
        error = "There are disconnected places.";
        return;
      }

      // an arc of incorrect place
      places_of_arcs = uniq( places_of_arcs );
      names_of_places = uniq( names_of_places );
      common_places = intersection( places_of_arcs, names_of_places );
      if ( places_of_arcs.length > common_places.length ) {
        error = "There are some missing places for arcs.";
        return;
      }

      // an arc without a place
      if ( any( arcs, (arc) => ! arc.hasOwnProperty('place') ) ) {
        error = "There are arcs without a place.";
        return;
      }

      // an arc without a transition
      if ( any( arcs, (arc) => ! arc.hasOwnProperty('transition')) ) {
        error = "There are arcs without a transition.";
        return;
      }

      // an arc with incorrect evaluator
      if ( any( arcs, (arc) => {
        if ( whatIs( arc.evaluate ) !== 'object' )
          return true;
        if ( ! arc.evaluate.hasOwnProperty('ready') )
          return true;
        if ( arc.evaluate.hasOwnProperty('time') ) {
          if ( whatIs( arc.evaluate.time ) !== 'number' )
            return true;
          if ( Math.floor( arc.evaluate.time ) !== arc.evaluate.time )
            return true;
          if ( arc.evaluate.time < 1 )
            return true;
        }
        if ( arc.evaluate.hasOwnProperty('place') )
          return false;
        if ( arc.evaluate.hasOwnProperty('global') )
          return false;
        if ( arc.evaluate.hasOwnProperty('update') ) {
          if ( arc.evaluate.update.hasOwnProperty('place') )
            return false;
          if ( arc.evaluate.update.hasOwnProperty('global') )
            return false;
          return true;
        }
        return true;
      }) ) {
        error = "There are arcs with incorrect evaluators.";
        return;
      }

      var trans_of_arcs = arcs.map( (a) => a.transition );
      var names_of_trans = transitions.map( (t) => t.name );
      var common_trans = intersection( trans_of_arcs, names_of_trans );
      // a transition without an arc
      if ( transitions.length > common_trans ) {
        error = "There are disconnected transitions.";
        return;
      }

      // an arc of incorrect transition
      trans_of_arcs = uniq( trans_of_arcs );
      names_of_trans = uniq( names_of_trans );
      common_trans = intersection( trans_of_arcs, names_of_trans );
      if ( trans_of_arcs.length > common_trans.length ) {
        error = "There are some missing transitions for arcs.";
        return;
      }

      // checking doubled arcs
      var signatures = arcs.map( (arc) => arc.place + arc.transition );
      var unique = uniq( signatures );
      if ( unique.length < signatures.length ) {
        error = "There are some doubled arcs.";
        return;
      }
    };

    // Returns the array of all arcs bound with the transition of the
    // given name.
    var arcsOfTransition = (t_name) => {
      return arcs.filter( (arc) => arc.transition === t_name );
    };

    // Returns the place objects, that the given arc object is bound
    // to.
    var placeOfArc = (arc) => {
      return find( places, (place) => place.name === arc.place );
    };

    // Returns the value of the arc's ready evaluation expression.
    // The expression is either a value, or a function.
    // When the expression is a function, it is called with two
    // arguments: the arc's place token and the global token. Whatever
    // the function returns, is then returned by this function.
    var arcReady = (arc) => {
      let place = placeOfArc( arc );
      // seems to be incorrect
      // if ( (place.token === undefined) && (token === undefined) )
      //  return false;
      let value;
      try {
        if ( whatIs( arc.evaluate.ready ) === 'function' )
          value = arc.evaluate.ready( clone(place.token), clone(token) );
        else
          value = arc.evaluate.ready;
        return value;
      }
      catch (e) {
        error = e.toString();
        return false;
      }
    };

    // Evaluates the given arc object value.
    // This function is too complex, and it should be refactored.
    var arcValue = (arc) => {
      let place = placeOfArc( arc );
      let value = {};
      try {
        // timed values
        if ( arc.evaluate.hasOwnProperty('time') ) {
          value.time = arc.evaluate.time;

          // token changes
          if ( arc.evaluate.hasOwnProperty('place') )
            value.place = arc.evaluate.place;
          if ( arc.evaluate.hasOwnProperty('global') )
            value.global = arc.evaluate.global;

          // updates
          if ( arc.evaluate.hasOwnProperty('update') ) {
            value.update = {};
            if ( arc.evaluate.update.hasOwnProperty('place') )
              value.update.place = arc.evaluate.update.place;
            if ( arc.evaluate.update.hasOwnProperty('global') )
              value.update.global = arc.evaluate.update.global;
          }
        }

        // not-timed values
        else {
          // token changes
          if ( arc.evaluate.hasOwnProperty('place') )
            value.place = arc.evaluate.place( clone(place.token), clone(token) );
          if ( arc.evaluate.hasOwnProperty('global') )
            value.global = arc.evaluate.global( clone(token) );

          // updates
          if ( arc.evaluate.hasOwnProperty('update') ) {
            value.update = {};
            if ( arc.evaluate.update.hasOwnProperty('place') )
              value.update.place = arc.evaluate.update.place( clone(place.token), clone(token) );
            if ( arc.evaluate.update.hasOwnProperty('global') )
              value.update.global = arc.evaluate.update.global( clone(token) );
          }
        }

        return value;
      }
      catch (e) {
        error = e.toString();
        return false;
      }
    };

    // For the given transition object, returns true, iff the
    // transition is ready.
    var isReady = (t) => {
      var the_arcs = arcsOfTransition(t.name);
      return all( the_arcs, (arc) => arcReady(arc) );
    };

    // Processes all the transitions "awaiting" for the "not-ready"
    // state. For each transition, that is not ready, the awaiting
    // state is changed to "ready".
    // This function is used to monitor transitions, which have the
    // onReady event.
    var checkAwaiting = () => {
      let awaiting = transitions.filter( (t) => t.$awaiting === 'not-ready' ),
          not_ready = awaiting.filter( (t) => ! isReady(t) );
      not_ready.forEach( (t) => t.$awaiting = 'ready' );
    };

    // Returns the next event identifier.
    var getEventId = () => {
      let id = event_id;
      event_id += 1;
      return id;
    };

    // Adds to the events queue the transition onTick event.
    //   handler.handler - the event handler, taken from the net
    //     specification
    //   handler.transition - the transition object
    //
    //   spec.type = 'ready'
    var queueTransitionOnReady = (handler, spec) => {
      events.transition.push({
        id: getEventId(),
        priority: 5,
        data: { handler, spec }
      });
    };

    // Adds to the events queue the transition onTick event.
    //   handler.handler - the event handler, taken from the net
    //     specification
    //   handler.transition - the transition object
    //
    //   spec.type = 'tick'
    var queueTransitionOnTick = (handler, spec) => {
      events.transition.push({
        id: getEventId(),
        priority: 1,
        data: { handler, spec }
      });
    };

    // Adds to the events queue the transition onFired event.
    //   handler.handler - the event handler, taken from the net
    //     specification
    //   handler.transition - the transition object
    //
    //   spec.type = 'fired'
    var queueTransitionOnFired = (handler, spec) => {
      events.transition.push({
        id: getEventId(),
        priority: 10,
        data: { handler, spec }
      });
    };

    // Adds to the events queue the place onUpdate event.
    //   handler.handler - the event handler, taken from the net
    //     specification
    //   handler.place - the place object
    //
    //   spec.type = 'update'
    var queuePlaceOnUpdate = (handler, spec) => {
      events.place.push({
        id: getEventId(),
        priority: 9,
        data: { handler, spec }
      });
    };

    // Adds a new event to the events queue.
    //   handler.handler: the handler for the event, taken from the
    //     net specification
    //   handler.place: the place object for the event
    //   handler.transition: the transition for the event
    //
    //   spec.type:
    //     'ready' for transition onReady
    //     'tick' for transition onTick
    //     'fired' for transition onFired
    //     'update' for place onUpdate
    var queueEvent = (handler, spec) => {
      switch (spec.type) {
        case 'ready':
          queueTransitionOnReady(handler, spec);
          break;
        
        case 'tick':
          queueTransitionOnTick(handler, spec);
          break;

        case 'fired':
          queueTransitionOnFired( handler, spec );
          break;

        case 'update':
          queuePlaceOnUpdate( handler, spec );
          break;

        default:
          throw new Error( `Unknown event type: ${spec.type}` );
      }
    };

    var handlePlaceEvent = (info, spec = {}) => {

      // handler must be a function
      if ( whatIs(info.handler) !== 'function' )
        throw new Error( `The onUpdate handler for the place ${info.place.name} is not a function.` );

      var token = getPlace(info.place.name).token;
      info.handler(token);
    };

    // Handles the given transition event.
    //   info.handler - the event handler
    //   info.transition - the transition object
    //   spec.processFired - ???
    //   spec.filter - ???
    var handleTransitionEvent = (info, spec = {}) => {

      if ( spec.hasOwnProperty('processFired') )
        spec.processFired(info.transition);

      // handler is a function
      if ( whatIs(info.handler) === 'function' ) {
        info.handler();
        return;
      }

      // handler is a transition name to fire
      let arr = [];
      if ( whatIs(info.handler) === 'string' )
        arr.push( info.handler );
      else
        arr = info.handler;

      // get the list of transitions
      arr = arr.map( (name) => find( transitions, (t) => t.name === name ) );

      // process the list, if needed
      if ( spec.hasOwnProperty('filter') )
        arr = spec.filter( arr );

      // try to fire them
      arr.forEach( (t) => {
        if ( isReady(t) )
          fire(t.name); // eslint-disable-line no-use-before-define
      });
    };

    // Handles a single event of the specified type:
    //   spec.type = 'transition', 'place'
    //   spec.handler = the function to handle event
    var handleEvent = (spec) => {
      let evts = events[spec.type];

      // get the highest priority
      let priority = min( evts, 'priority' ).priority,
      // get the most important events from the queue
          the_events = evts.filter( (e) => e.priority === priority );

      // remove the first event
      let evt = the_events.pop();
      events[spec.type] = evts.filter( (e) => e.id !== evt.id );

      // handle the first event
      spec.handler( evt.data.handler, evt.data.spec );
    };

    // The main function for events handling.
    // Once invoked, cannot be invoked again, before the first one is
    // finished. (The `events_handled` variable is used for that
    // purpose.)
    // Handles all the events in the queue.
    var handleEvents = () => {
      // don't let this function to be called many times
      if ( events_handled === true )
        return;

      // set the flag to stop further calls to this function
      events_handled = true;

      // handle all the transition events
      while ( events.transition.length > 0 )
        handleEvent({
          type:    'transition',
          handler: handleTransitionEvent
        });

      // handle all the place events
      while ( events.place.length > 0 )
        handleEvent({
          type:    'place',
          handler: handlePlaceEvent
        });

      // set the flag to allow further calls to this function
      events_handled = false;
    };

    var checkTransitionEvent = (spec) => {
      const events = spec.getEvents(); // eslint-disable-line no-shadow
      events.forEach( (handler) => queueEvent( handler, spec ) );
    };

    var checkOnReady = function() {
      checkTransitionEvent({
        type:         'ready',
        getEvents:    readyEvents,
        filter:       (arr) => arr.filter( (t) => isReady(t) ),
        processFired: (t) => t.$awaiting = 'not-ready'
      });
    };

    var checkOnTick = () => {
      checkTransitionEvent({
        type:      'tick',
        getEvents: tickEvents
      });
    };

    // public functions
    var getTime = () => time;
    var tick = () => {
      time += 1;

      // update the global token
      let g_times = partition( global_time, (t) => t.time === time );
      g_times[0].forEach( (t) => {
        if ( t.hasOwnProperty('func') )
          token = t.func( clone(t.old_token) );
        if ( t.hasOwnProperty('update') )
          merge( token, t.update( clone(token) ) );
      });
      global_time = g_times[1];

      // update tokens in all places
      places.forEach( (place) => {
        if ( ! place.hasOwnProperty('time') )
          return;

        let times = partition( place.time, (t) => t.time === time );

        // process all the times === time
        times[0].forEach( (t) => {
          let changed = false;
          if ( t.hasOwnProperty('func') ) {
            place.token = t.func( clone(t.old_token), clone(token) );
            changed = true;
          }
          if ( t.hasOwnProperty('update') ) {
            merge( place.token, t.update( clone(place.token), clone(token) ) );
            changed = true;
          }
          if ( changed )
            checkPlaceOnUpdate(place);
        });

        place.time = times[1];
      });

      checkOnReady();
      checkOnTick();

      handleEvents();
    };

    var getToken = () => clone(token);

    var readyTransitions = () => {
      var ready = transitions.filter( (t) => isReady(t) );
      return ready.map( (t) => t.name );
    };

    var isTransReady = (name) => {
      // find the transition
      let trans = transitions.filter( (t) => t.name === name );

      // a transition, which doesn't exist, can't be ready (???)
      if ( trans.length === 0 )
        return false;

      return isReady( trans[0] );
    };

    // private
    var readyEvents = () => {
      var ready = transitions.filter( (t) => (t.$awaiting === 'ready') && isReady(t) );
      return ready.map( (t) => { return { handler: t.onReady, transition: t }; } );
    };

    var tickEvents = () => {
      var trans = transitions.filter( (t) => t.onTick !== undefined );
      return trans.map( (t) => { return { handler: t.onTick, transition: t }; } );
    };

    var checkPlaceOnUpdate = (place) => {
      if ( place.hasOwnProperty('onUpdate') )
        queueEvent( { handler: place.onUpdate, place }, { type: 'update' } );
    };

    var fire = (name) => {
      var trans = find( transitions, (t) => t.name === name );
      if ( trans === undefined )
        throw Error('No transition ' + name);
      if ( ! isReady(trans) )
        error = "The transition " + name + "is not ready.";
      else {
        let the_arcs = arcsOfTransition(name);
        the_arcs.forEach( (arc) => {
          let value = arcValue( arc );
          let place = find( places, (p) => p.name === arc.place );

          if ( value.hasOwnProperty('time') ) {
            let token_time = time + value.time;
            
            let place_info = {
              time: token_time
            };
            let global_info = {
              time: token_time
            };

            // timed token for place
            if ( value.hasOwnProperty('place') ) {
              if ( ! place.hasOwnProperty('time') )
                place.time = [];
              merge( place_info, {
                func: value.place,
                old_token: place.token
              });
              delete place['token'];
              checkPlaceOnUpdate(place);
            }

            // timed update for place
            if ( value.hasOwnProperty('update') && value.update.hasOwnProperty('place') ) {
              if ( ! place.hasOwnProperty('time') )
                place.time = [];
              place_info.update = value.update.place;
            }

            if ( place_info.hasOwnProperty('func') || place_info.hasOwnProperty('update') )
              place.time.push(place_info);
            
            // timed global token
            if ( value.hasOwnProperty('global') ) {
              merge( global_info, {
                func: value.global,
                old_token: token
              });
              token = undefined;
            }

            // times update for the global token
            if ( value.hasOwnProperty('update') && value.update.hasOwnProperty('global') ) {
              global_info.update = value.update.global;
            }

            if ( global_info.hasOwnProperty('func') || global_info.hasOwnProperty('update') )
              global_time.push( global_info );
          }
          else {
            if ( value.hasOwnProperty('place') ) {
              place.token = value.place;
              checkPlaceOnUpdate( place );
            }

            if ( value.hasOwnProperty('global') )
              token = value.global;

            if ( value.hasOwnProperty('update') ) {
              if ( value.update.hasOwnProperty('place') ) {
                merge( place.token, value.update.place );
                checkPlaceOnUpdate( place );
              }
              if ( value.update.hasOwnProperty('global') )
                merge( token, value.update.global );
            }
          }

        });

        checkAwaiting();

        checkOnReady();

        if ( trans.hasOwnProperty('onFired') ) {
          queueEvent( { handler: trans.onFired, transition: trans }, { type: 'fired' } );
        }

        handleEvents();
      }
    };

    var getPlaces = () => {
      return places.map( (place) => clone(place) );
    };

    var getPlace = (name) => {
      var place = find( places, (p) => p.name === name );
      if ( place !== undefined )
        return clone( place );
      else
        throw Error('No place ' + name);
    };

    var getError = () => error;

    var reset = () => initNet();

    //private
    var initNet = () => {
      places = getDefault( conf.places, [] );
      token = getDefault( conf.token, undefined );
      arcs = getDefault( conf.arcs, [] );
      transitions = getDefault( conf.transitions, [] );
      time = 0;
      global_time = [];
      events = {
        transition: [],
        place: []
      };
      event_id = 0;
      events_handled = false;

      // set the $awaiting for transitions
      let on_ready_trans = transitions.filter( (t) => t.hasOwnProperty('onReady') );
      on_ready_trans.forEach( (t) => t.$awaiting = 'ready' );

      //fireAutoTransitions();
      checkOnReady();

      handleEvents();
    };
    
    // the initialisation

    initNet();
    checkNet();

    return {
      getTime:          getTime,
      tick:             tick,
      getToken:         getToken,
      readyTransitions: readyTransitions,
      isReady:          isTransReady,
      fire:             fire,
      getPlaces:        getPlaces,
      getPlace:         getPlace,
      getError:         getError,
      reset:            reset
    };
  })();
};

exports.Net = Net;

