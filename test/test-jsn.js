var mocha  = require('mocha'),
    should = require('should'),
    sinon  = require('sinon'),
    _      = require('lodash'),
    jsn    = require('../build/jsn');

describe('JSN engine', function() {

  it( 'empty net', function() {
    var net = jsn.Net();

    // the time should be 0, and it should be possible to tick
    net.getTime().should.eql(0);
    net.tick();
    net.getTime().should.eql(1);

    // empty global token
    var token = net.getToken();
    (token === undefined).should.be.true;

    // no transitions, no places
    (net.readyTransitions().length).should.eql(0);
    (net.getPlaces().length).should.eql(0);

    // no errors
    (net.getError() === undefined).should.be.true;

    // getting a place or firing a transition should throw
    net.should.have.property('getPlace');
    net.getPlace.should.be.a.Function;
    net.should.have.property('fire');
    net.fire.should.be.a.Function;
    (function() {net.getPlace('x');}).should.throw();
    (function() {net.fire('x');}).should.throw();
  });

  it('token', function() {
    var net = jsn.Net({
      token: { alpha: 'beta' }
    });

    // get the token and verify it
    var token = net.getToken();
    token.should.have.property('alpha', 'beta');

    // modify the returned object; the token should remain unmodified
    token.alpha = 'ooops!';
    token = net.getToken();
    token.should.have.property('alpha', 'beta');
  });

  describe( 'validation', function() {

    var evaluate = {
      ready: true,
      place: 0
    };
    
    it('disconnected place', function() {
      var net = jsn.Net({
        places: [
          { name: 'x' }
        ]
      });

      var err = net.getError();
      (err !== undefined).should.be.true;
    });

    it('disconnected transition', function() {
      var net = jsn.Net({
        transitions: [
          { name: 'x' }
        ]
      });

      var err = net.getError();
      (err !== undefined).should.be.true;
    });

    it('arc with incomplete data 1', function() {
      var net = jsn.Net({
        places: [
          { name: 'x' }
        ],

        transitions: [
          { name: 'doX' }
        ],

        arcs: [
          { place: 'x', evaluate: evaluate }
        ]
      });

      var err = net.getError();
      (err !== undefined).should.be.true;
    });

    it('arc with incomplete data 2', function() {
      var net = jsn.Net({
        places: [
          { name: 'x' }
        ],

        transitions: [
          { name: 'doX' }
        ],

        arcs: [
          { transition: 'doX', evaluate: evaluate }
        ]
      });

      err = net.getError();
      (err !== undefined).should.be.true;
    });

    it('arc with incomplete data 3', function() {

      var net = jsn.Net({
        places: [
          { name: 'x' }
        ],

        transitions: [
          { name: 'doX' }
        ],

        arcs: [
          { transition: 'doX', place: 'x' }
        ]
      });

      err = net.getError();
      (err !== undefined).should.be.true;
    });

    it('arc of incorrect place', function() {
      var net = jsn.Net({
        places: [
          { name: 'x' }
        ],

        transitions: [
          { name: 'doX' }
        ],

        arcs: [
          { place: 'y', transition: 'doX', evaluate: evaluate },
          { place: 'x', transition: 'doX', evaluate: evaluate }
        ]
      });
      
      var err = net.getError();
      (err !== undefined).should.be.true;
    });

    it('arc of incorrect transition', function() {
      var net = jsn.Net({
        places: [
          { name: 'x' }
        ],

        transitions: [
          { name: 'doX' }
        ],

        arcs: [
          { place: 'x', transition: 'doY', evaluate: evaluate },
          { place: 'x', transition: 'doX', evaluate: evaluate }
        ]
      });
      
      var err = net.getError();
      (err !== undefined).should.be.true;
    });

    it('more than one arc for a place and a transition', function() {
      var net = jsn.Net({
        places: [
          { name: 'x' }
        ],

        transitions: [
          { name: 'doX' }
        ],

        arcs: [
          { place: 'x', transition: 'doX', evaluate: evaluate },
          { place: 'x', transition: 'doX', evaluate: evaluate }
        ]
      });

      var err = net.getError();
      (err !== undefined).should.be.true;
    });

    it( 'arc evaluate should have ready', function() {

      var net = jsn.Net({
        places: [
          { name: 'x' }
        ],

        transitions: [
          { name: 'doX' }
        ],

        arcs: [
          { place: 'x', transition: 'doX', 
            evaluate: {
              place: function() { return 1; }
          } }
        ]
      });
      
      var err = net.getError();
      (err !== undefined).should.be.true;

    });

    it( 'arc evaluate should have at least one of: place', function() {

      var net = jsn.Net({
        places: [
          { name: 'x' }
        ],

        transitions: [
          { name: 'doX' }
        ],

        arcs: [
          { place: 'x', transition: 'doX', 
            evaluate: {
              ready: true
          } }
        ]
      });
      
      var err = net.getError();
      (err !== undefined).should.be.true;
      
      net = jsn.Net({
        places: [
          { name: 'x' }
        ],

        transitions: [
          { name: 'doX' }
        ],

        arcs: [
          { place: 'x', transition: 'doX', 
            evaluate: {
              ready: true,
              place: function() { return 1; }
          } }
        ]
      });
      
      err = net.getError();
      (err === undefined).should.be.true;

    });

    it( 'arc evaluate should have at least one of: global', function() {

      var net = jsn.Net({
        places: [
          { name: 'x' }
        ],

        transitions: [
          { name: 'doX' }
        ],

        arcs: [
          { place: 'x', transition: 'doX', 
            evaluate: {
              ready: true
          } }
        ]
      });
      
      var err = net.getError();
      (err !== undefined).should.be.true;
      
      net = jsn.Net({
        places: [
          { name: 'x' }
        ],

        transitions: [
          { name: 'doX' }
        ],

        arcs: [
          { place: 'x', transition: 'doX', 
            evaluate: {
              ready: true,
              global: function() { return 1; }
          } }
        ]
      });
      
      err = net.getError();
      (err === undefined).should.be.true;

    });
    
    it( 'arc evaluate should have at least one of: update place', function() {

      var net = jsn.Net({
        places: [
          { name: 'x' }
        ],

        transitions: [
          { name: 'doX' }
        ],

        arcs: [
          { place: 'x', transition: 'doX', 
            evaluate: {
              ready: true
          } }
        ]
      });
      
      var err = net.getError();
      (err !== undefined).should.be.true;
      
      net = jsn.Net({
        places: [
          { name: 'x' }
        ],

        transitions: [
          { name: 'doX' }
        ],

        arcs: [
          { place: 'x', transition: 'doX', 
            evaluate: {
              ready: true,
              update: {
                place: function() { return 1; }
              }
          } }
        ]
      });
      
      err = net.getError();
      (err === undefined).should.be.true;

    });

    it( 'arc evaluate should have at least one of: update global', function() {

      var net = jsn.Net({
        places: [
          { name: 'x' }
        ],

        transitions: [
          { name: 'doX' }
        ],

        arcs: [
          { place: 'x', transition: 'doX', 
            evaluate: {
              ready: true
          } }
        ]
      });
      
      var err = net.getError();
      (err !== undefined).should.be.true;
      
      net = jsn.Net({
        places: [
          { name: 'x' }
        ],

        transitions: [
          { name: 'doX' }
        ],

        arcs: [
          { place: 'x', transition: 'doX', 
            evaluate: {
              ready: true,
              update: {
                global: function() { return 1; }
              }
          } }
        ]
      });
      
      err = net.getError();
      (err === undefined).should.be.true;

    });
    
    it( 'arc evaluate - time should be positive integer', function() {

      var net = jsn.Net({
        places: [
          { name: 'x' }
        ],

        transitions: [
          { name: 'doX' }
        ],

        arcs: [
          { place: 'x', transition: 'doX', 
            evaluate: {
              ready: true,
              place: function() { return 1; },
              time: 1
          } }
        ]
      });
      
      var err = net.getError();
      (err === undefined).should.be.true;

      net = jsn.Net({
        places: [
          { name: 'x' }
        ],

        transitions: [
          { name: 'doX' }
        ],

        arcs: [
          { place: 'x', transition: 'doX', 
            evaluate: {
              ready: true,
              place: function() { return 1; },
              time: "1"
          } }
        ]
      });
      
      var err = net.getError();
      (err !== undefined).should.be.true;

      net = jsn.Net({
        places: [
          { name: 'x' }
        ],

        transitions: [
          { name: 'doX' }
        ],

        arcs: [
          { place: 'x', transition: 'doX', 
            evaluate: {
              ready: true,
              place: function() { return 1; },
              time: true
          } }
        ]
      });
      
      var err = net.getError();
      (err !== undefined).should.be.true;
      
    });

  }); // validation
  
  describe( 'count up', function() {
    
    var net;

    beforeEach( function() {
      net = jsn.Net({
        places: [
          { name: 'place', token: 0 }
        ],

        transitions: [
          { name: 'count' }
        ],

        arcs: [
          { place: 'place', transition: 'count',
            evaluate: {
              ready: true,
              place: function(token) { return token + 1; }
            } }
        ]
      });
    });

    var checkPlace = function( place, value ) {
      place.should.be.an.Object;
      place.should.have.property('name', 'place');
      place.should.have.property('token', value);
    };

    var checkReady = function( net ) {
      var ready = net.readyTransitions();

      ready.should.be.an.Array;
      ready.length.should.eql(1);
      ready[0].should.eql('count');
    };

    it('getPlaces', function() {
      var places = net.getPlaces();
      places.length.should.eql(1);
      checkPlace( places[0], 0 );

      // modifying the places shouldn't affect the net
      places[0].name = 'alpha';
      places[0].token = 2;
      places = net.getPlaces();
      places.length.should.eql(1);
      checkPlace( places[0], 0 );      
    });

    it('getPlace', function() {
      var place = net.getPlace('place');
      checkPlace( place, 0 );

      // modifying the object shouldn't affect the net
      place.name = 'alpha';
      place.token = 2;
      place = net.getPlace('place');
      checkPlace( place, 0 );
    });

    it('readyTransitions', function() {
      checkReady( net );
    });

    it('isReady', function() {
      (net.isReady('count')).should.be.true;
    });

    it('counting up', function() {
      net.fire('count');

      // the transition should be ready to fire
      checkReady( net );

      // the counter should be 1
      checkPlace( net.getPlace('place'), 1 );
    });

    it('reset', function() {
      net.fire('count');
      net.reset();

      checkReady( net );
      checkPlace( net.getPlace('place'), 0 );
    });
    
  }); // count up

  describe( 'Simple nets', function() {

    var checkTrans = function( net, arr ) {
      var ready = net.readyTransitions();
      ready.should.be.an.Array;
      ready.length.should.eql(arr.length);
      _.intersection( ready, arr ).length.should.eql( arr.length );
    };
    
    it('countdown', function() {
  
  
      var net = jsn.Net({
        places: [
          { name: 'place', token: 1 }
        ],
  
        transitions: [
          { name: 'count' }
        ],
  
        arcs: [
          { place: 'place', transition: 'count',
            evaluate: {
              ready: function(token) { return token > 0; },
              place: function(token) { return token - 1; }
            } }
        ]
      });
  
      // the transition should be ready
      checkTrans( net, ['count'] );
      (net.isReady('count')).should.be.true;
  
      // fire transition
      net.fire( 'count' );
  
      // the token should be modified
      var place = net.getPlace('place');
      place.should.have.property('token', 0);
      
      // the transition shouldn't be ready
      checkTrans( net, [] );
      (net.isReady('count')).should.be.false;
  
      // firing the transition should result in error
      (net.getError() === undefined).should.be.true;
      net.fire( 'count' );
      (net.getError() !== undefined).should.be.true;
  
      // reset
      net.reset();
  
      // the transition should be ready
      checkTrans( net, ['count'] );
      (net.isReady('count')).should.be.true;
    });
  
    it('global token', function() {
    
      var net = jsn.Net({
        places: [
          { name: 'place' }
        ],
  
        transitions: [
          { name: 'count' }
        ],
  
        arcs: [
          { place: 'place', transition: 'count',
            evaluate: {
              ready: function(place_token, global_token) { return global_token > 0; },
              global: function(global_token) { return global_token - 1; }
            } }
        ],

        token: 1
      });

      // the transition should be ready
      checkTrans( net, ['count'] );

      // the global token should be equal to 1
      net.getToken().should.eql(1);

      // fire the transition
      net.fire( 'count' );

      // the global token should've been modified
      net.getToken().should.eql(0);

      // the transition shouldn't be ready anymore
      checkTrans( net, [] );

      // reset the net
      net.reset();

      // the net should behave the same as after it was created
      checkTrans( net, ['count'] );
      net.getToken().should.eql(1);
    });

    it('time in place', function() {
     
      var net = jsn.Net({
        places: [
          { name: 'place', token: 0 }
        ],
  
        transitions: [
          { name: 'count' }
        ],
  
        arcs: [
          { place: 'place', transition: 'count',
            evaluate: {
              ready: function(token) { return token !== undefined; },
              place: function(token) { return token + 1; },
              time: 1
            } }
        ],

        time: 0
      });

      // the transition should be ready
      checkTrans( net, ['count'] );

      // the token in the place should be equal to 1
      var place = net.getPlace('place');
      place.should.have.property('token', 0);

      // the net time should be 0
      net.getTime().should.eql(0);
      
      // fire transition
      net.fire('count');

      // the net time should be still 0
      net.getTime().should.eql(0);

      // the transition shouldn't be ready anymore
      checkTrans( net, [] );

      // the token in the place should have the proper timestamp
      place = net.getPlace('place');
      place.should.not.have.property('token');
      place.should.have.property('time');
      place.time.should.be.an.Array;
      place.time.length.should.eql(1);
      place.time[0].should.have.property('time', 1);
      place.time[0].should.have.property('func');

      // tick
      net.tick();

      // the net time should be equal to 1
      net.getTime().should.eql(1);

      // the place should have the proper token
      place = net.getPlace('place');
      place.should.have.property('token', 1);
      place.should.have.property('time');
      place.time.should.be.an.Array;
      place.time.length.should.eql(0);

      // the transition should be prepared
      checkTrans( net, ['count'] );
    });

    it('time in global', function() {
     
      var net = jsn.Net({
        places: [
          { name: 'place' }
        ],
  
        transitions: [
          { name: 'count' }
        ],
  
        arcs: [
          { place: 'place', transition: 'count',
            evaluate: {
              ready: function(t, g) { return g !== undefined; },
              global: function(g) { return g + 1; },
              time: 1
            } }
        ],

        time: 0,
        token: 0
      });

      // the transition should be ready
      checkTrans( net, ['count'] );

      // the global token should be equal to 1
      var place = net.getToken();
      place.should.eql(0);

      // the net time should be 0
      net.getTime().should.eql(0);
      
      // fire transition
      net.fire('count');

      // the net time should be still 0
      net.getTime().should.eql(0);

      // the transition shouldn't be ready anymore
      checkTrans( net, [] );

      // the global token shouldn't be available anymore
      place = net.getToken();
      (place === undefined).should.be.true;

      // tick
      net.tick();

      // the net time should be equal to 1
      net.getTime().should.eql(1);

      // the global token should be equal to one
      place = net.getToken();
      place.should.eql(1);

      // the transition should be prepared
      checkTrans( net, ['count'] );
    });
    
    // the tests seems to be incorrect
    it.skip('two products of different times', function() {

      var prodToken = {
        ready: function(token) { return (token !== undefined) && (token > 0); },
        place: function(token) { return token - 1; }
      };
      
      var checkDest = function( net, value1, value2 ) {
        var place = net.getPlace('dest');
        place.should.have.property('token');
        place.token.should.have.property('prod1', value1);
        place.token.should.have.property('prod2', value2);
      };

      // create a net
      var net = jsn.Net({
        places: [
          { name: 'source1', token: 2 },
          { name: 'source2', token: 1 },
          { name: 'dest', token: { prod1: 0, prod2: 0 } }
        ],
  
        transitions: [
          { name: 'prod1' },
          { name: 'prod2' }
        ],
  
        arcs: [
          { place: 'source1', transition: 'prod1',
            evaluate: prodToken },

          { place: 'source2', transition: 'prod2',
            evaluate: prodToken },

          { place: 'dest', transition: 'prod1',
            evaluate: {
              ready: true,
              place: function(token) { return {
                prod1: token.prod1 + 1,
                prod2: token.prod2
              }; },
              time: 1
            } },

          { place: 'dest', transition: 'prod2',
            evaluate: {
              ready: true,
              place: function(token) { return {
                prod1: token.prod1,
                prod2: token.prod2 + 1
              }; },
              time: 2
            } }
        ],

        time: 0
      });
      
      // all transitions should be ready
      checkTrans( net, ['prod1', 'prod2'] );

      // fire the prod1 transition
      net.fire( 'prod1' );

      // all the transitions should be ready
      checkTrans( net, ['prod1', 'prod2'] );

      // fire the prod1 transition
      net.fire( 'prod1' );

      // the source1 place should be empty
      var place = net.getPlace( 'source1' );
      place.should.have.property('token', 0);

      // the dest place should be {0, 0}
      checkDest( net, 0, 0 );

      // the source2 place should still have its token
      place = net.getPlace( 'source2' );
      place.should.have.property('token', 1);

      // only the transition prod2 should be ready
      checkTrans( net, ['prod2'] );

      // fire the transition prod2
      net.fire( 'prod2' );

      // the source2 place should be empty
      place = net.getPlace( 'source2' );
      place.should.have.property('token', 0);

      // the dest place should be {0, 0}
      checkDest( net, 0, 0 );

      // tick
      net.tick();
      
      // the dest place should be {2, 0}
      checkDest( net, 2, 0 );

      // no transitions should be ready
      checkTrans( net, [] );

      // tick
      net.tick();

      // the dest place should be {2, 1}
      checkDest( net, 2, 1 );

      // no transitions should be ready
      checkTrans( net, [] );
    });

    it('throwing arc', function() {

      // create a net
      var net = jsn.Net({
        places: [
          { name: 'place' }
        ],

        transitions: [
          { name: 'action' }
        ],

        arcs: [
          { place: 'place', transition: 'action',
            evlauate: {
              ready: function() { throw Error('Ooops'); },
              value: 1
            } }
        ]
      });

      // check, whether the transition is ready
      checkTrans( net, [] );

      // the error should be set
      (net.getError() !== undefined).should.be.true;
    });

    // it seems that this test is now obsolete
    it.skip('global token modified after tick', function() {

      // create net
      var net = jsn.Net({
        places: [
          { name: 'place' }
        ],

        transitions: [
          { name: 'action' }
        ],

        arcs: [
          { place: 'place', transition: 'action',
            evaluate: {
              ready: true,
              global: function(global) { return global + 1; },
              time: 1
            } }
        ],

        token: 1,

        time: 0
      });

      // the transition should be ready
      checkTrans( net, ['action'] );

      // fire the transition
      net.fire( 'action' );

      // the global token should be unmodified
      var glob = net.getToken();
      glob.should.eql(1);

      // tick
      net.tick();

      // the global token should've been modified
      net.getToken().should.eql(2);
    });
      
  }); // Simple nets

  describe( 'place events', function() {
    
    var spec;

    beforeEach( function() {
      spec = {
        places: [
          { name: 'place', token: { one: 1 } }
        ],

        transitions: [
          { name: 'set' },
          { name: 'update' }
        ],

        arcs: [
          { place: 'place', transition: 'set',
            evaluate: {
              ready: true,
              place: function(t) { return { one: 'one' }; }
            } },

          { place: 'place', transition: 'update',
            evaluate: {
              ready: true,
              update: {
                place: function(t) { return { two: 2 }; }
              }
            } }
        ]
      };
    });

    it('onUpdate on set', function(done) {
      
      // set up the callback
      spec.places[0].onUpdate = function(token) {
        token.should.have.property('one', 'one');
        var info = net.getPlace('place').token;
        info.should.have.property('one', 'one');
        done();
      };

      // create the net
      var net = jsn.Net(spec);

      // fire the set transition
      net.fire('set');
      
    });

    it( 'onUpdate on update', function(done) {
      
      // set up the callback
      spec.places[0].onUpdate = function(token) {
        token.should.have.property('one', 1);
        token.should.have.property('two', 2);
        var info = net.getPlace('place').token;
        info.should.have.property('one', 1);
        info.should.have.property('two', 2);
        done();
      };

      // create the net
      var net = jsn.Net(spec);

      // fire the transition
      net.fire('update');

    }); // onUpdate on update

    it( 'onUpdate on timed set - undefined', function(done) {

      // set up the callback
      spec.places[0].onUpdate = function(token) {
        (token === undefined).should.be.true;
        var info = net.getPlace('place').token;
        (info === undefined).should.be.true;
        done();
      };

      // create the net
      spec.arcs[0].evaluate.time = 1;
      var net = jsn.Net(spec);

      // fire the transition
      net.fire('set');

    }); // onUpdate on timed set - undefined
    
    it( 'onUpdate on timed set', function(done) {

      var called = false;

      // set up the callback
      spec.places[0].onUpdate = function(token) {
        if ( ! called ) {
          called = true;
          return;
        }

        token.should.have.property('one', 'one');
        var info = net.getPlace('place').token;
        info.should.have.property('one', 'one');
        done();
      };

      // create the net
      spec.arcs[0].evaluate.time = 1;
      var net = jsn.Net(spec);

      // fire the transition
      net.fire('set');

      // tick
      net.tick();

    }); // onUpdate on timed set
    
    it( 'onUpdate on timed update', function(done) {

      // set up the callback
      spec.places[0].onUpdate = function(token) {
        token.should.have.property('one', 1);
        token.should.have.property('two', 2);
        var info = net.getPlace('place').token;
        info.should.have.property('one', 1);
        info.should.have.property('two', 2);
        done();
      };

      // create the net
      spec.arcs[1].evaluate.time = 1;
      var net = jsn.Net(spec);

      // fire the transition
      net.fire('update');

      // tick
      net.tick();

    }); // onUpdate on timed update
    
  }); // place events
  

  describe( 'transition events', function() {
    
    var spec, spec_not_ready;

    beforeEach( function() {
      spec = {
        places: [
          { name: 'place' }
        ],

        transitions: [
          { name: 'test' }
        ],

        arcs: [
          { place: 'place', transition: 'test',
            evaluate: {
              ready: true,
              place: function() { return 1; }
            } }
        ]
      };

      spec_not_ready = {
        places: [
          { name: 'place', token: 0 }
        ],

        transitions: [
          { name: 'first' },
          { name: 'second' }
        ],

        arcs: [
          { place: 'place', transition: 'first',
            evaluate: {
              ready: function(t) { return t === 0; },
              place: function(t) { return t + 1; }
            } },

          { place: 'place', transition: 'second',
            evaluate: {
              ready: function(t) { return t > 0; },
              place: function(t) { return t + 1; }
            } }
        ]
      };
    });

    // deprecated
    it.skip('onReady', function() {
      
      // create the callbacks
      var callback = sinon.spy();

      // set the callbacks
      spec.transitions[0].onReady = callback;

      // create the net
      var net = jsn.Net( spec );

      // the transition is ready, the callback shouldv'e been called
      (callback.called).should.be.true;
      (callback.calledOnce).should.be.true;

      // fire the transition
      net.fire( 'test' );

      // the transition should be still ready, and the callback should've been called once again
      callback.callCount.should.eql(2);

    });

    it('onFired', function() {

      // create the callback
      var callback = sinon.spy();

      // set the callback
      spec.transitions[0].onFired = callback;

      // create the net
      var net = jsn.Net( spec );

      // the callback shouldn't have been called so far
      callback.callCount.should.eql(0);

      // fire the transition
      net.fire( 'test' );

      // the callback should've been called once
      callback.callCount.should.eql(1);
    });

    it('onTick', function() {

      // create the callback
      var callback = sinon.spy();

      // set the callback
      spec.transitions[0].onTick = callback;

      // create the net
      var net = jsn.Net( spec );

      // the callback shouldn't have been called so far
      callback.callCount.should.eql(0);

      // fire the transition
      net.fire( 'test' );

      // the callback shouldn't have been called so far
      callback.callCount.should.eql(0);

      // tick
      net.tick();

      // the callback should've been called once
      callback.callCount.should.eql(1);

    });

    it('automatic fire on ready', function() {

      // set the automatic callback
      spec_not_ready.transitions[0].onReady = 'first';

      // create a net
      var net = jsn.Net( spec_not_ready );

      // the transition should've been fired
      var info = net.getPlace('place');
      info.token.should.eql(1);

      // set the automatic callback
      spec_not_ready.transitions[0].onReady = ['first'];

      // create a net
      net = jsn.Net( spec_not_ready );

      // the transition should've been fired
      info = net.getPlace('place');
      info.token.should.eql(1);

    });

    it('automatic fire on fired', function() {
      // set the automatic callback
      spec_not_ready.transitions[0].onFired = 'second';

      // create a net
      var net = jsn.Net( spec_not_ready );

      // the transition should not have been fired
      var info = net.getPlace('place');
      info.token.should.eql(0);

      // fire the first transition
      net.fire( 'first' );

      // the second transition should also have been fired
      info = net.getPlace('place');
      info.token.should.eql(2);

      // set the automatic callback
      spec_not_ready.transitions[0].onFired = ['second'];

      // create a net
      net = jsn.Net( spec_not_ready );

      // the transition should not have been fired
      info = net.getPlace('place');
      info.token.should.eql(0);

      // fire the first transition
      net.fire( 'first' );

      // the second transition should also have been fired
      info = net.getPlace('place');
      info.token.should.eql(2);
    });

    it('automatic fire on tick', function() {
      // set the automatic callback
      spec_not_ready.transitions[0].onTick = 'first';

      // create a net
      var net = jsn.Net( spec_not_ready );

      // the transition should not have been fired
      var info = net.getPlace('place');
      info.token.should.eql(0);

      // tick
      net.tick();

      // the transition should've been fired
      var info = net.getPlace('place');
      info.token.should.eql(1);
      
      // set the automatic callback
      spec_not_ready.transitions[0].onTick = ['first'];

      // create a net
      net = jsn.Net( spec_not_ready );

      // the transition should not have been fired
      var info = net.getPlace('place');
      info.token.should.eql(0);

      // tick
      net.tick();
      
      // the transition should've been fired
      info = net.getPlace('place');
      info.token.should.eql(1);
    });
    
    it('default events priorities', function() {
   
      var countdown = function(t) {
        return {
          timer: t.timer - 1
        };
      };

      var setTimer = function(t) {
        return {
          timer: 1
        };
      };

      var timerset = function(t) {
        return t.timer > 0;
      };

      var checkTimer = function( name, value ) {
        var info = net.getPlace(name);
        info.token.should.have.property('timer', value);
      };

      // create net
      var net = jsn.Net({
        places: [
          { name: 'place1', token: { timer: 1 } },
          { name: 'place2', token: { timer: 0 } },
          { name: 'place3', token: { timer: 0 } }
        ],
   
        transitions: [
          { name: 'count1', onTick: 'count1' },
          { name: 'setTimers', onReady: 'setTimers' },
          { name: 'count2', onTick: 'count2' },
          { name: 'count3', onTick: 'count3' }
        ],
   
        arcs: [
          { place: 'place1', transition: 'count1',
            evaluate: {
              ready: timerset,
              update: {
                place: countdown
              }
            } },

          { place: 'place1', transition: 'setTimers',
            evaluate: {
              ready: function(t) { return t.timer === 0; },
              place: function(t) { return t; }
            } },

          { place: 'place2', transition: 'setTimers',
            evaluate: {
              ready: true,
              update: {
                place: setTimer
              }
            } },

          { place: 'place3', transition: 'setTimers',
            evaluate: {
              ready: true,
              update: {
                place: setTimer
              }
            } },

          { place: 'place2', transition: 'count2',
            evaluate: {
              ready: timerset,
              update: {
                place: countdown
              }
            } },

          { place: 'place3', transition: 'count3',
            evaluate: {
              ready: timerset,
              update: {
                place: countdown
              }
            } }
        ]
      });

      // in place1 and place2 timer should be 0
      checkTimer( 'place1', 1 );
      checkTimer( 'place2', 0 );
      checkTimer( 'place3', 0 );

      // tick
      net.tick();

      // onTick should be called first
      // then, onReady should be called and timers in places 2 and 3 should be set to 1

      // check the timers
      checkTimer( 'place1', 0 );
      checkTimer( 'place2', 1 );
      checkTimer( 'place3', 1 );

      // tick
      net.tick();
      
      // check the timers
      checkTimer( 'place1', 0 );
      checkTimer( 'place2', 0 );
      checkTimer( 'place3', 0 );
    });

    it('onReady should be called once - auto', function() {
      
      // create a net
      var net = jsn.Net({
        places: [
          { name: 'place', token: { counter: 0, ready: 0 } },
        ],

        transitions: [
          { name: 'test', onReady: 'test' },
          { name: 'count' }
        ],

        arcs: [
          { place: 'place', transition: 'test',
            evaluate: {
              ready: true,
              update: {
                place: function(t) { return { ready: t.ready + 1 }; }
              }
            } },

          { place: 'place', transition: 'count',
            evaluate: {
              ready: true,
              update: {
                place: function(t) { return { counter: t.counter + 1 }; }
              }
            } }
        ]
      });

      // the onReady callback should've been called
      var info = net.getPlace('place');
      info.token.should.have.property('ready', 1);
      
      // fire the counter
      net.fire( 'count' );

      // the onReady callback should've been called once
      info = net.getPlace('place');
      info.token.should.have.property('ready', 1);
    });

    it('onReady should be called once', function() {
      
      // create the callback
      var callback = sinon.spy();

      // create a net
      var net = jsn.Net({
        places: [
          { name: 'place' }
        ],

        transitions: [
          { name: 'test', onReady: callback }
        ],

        arcs: [
          { place: 'place', transition: 'test',
            evaluate: {
              ready: true,
              place: function() { return 'ok'; }
            } }
        ]
      });

      // the onReady callback should've been called
      callback.called.should.be.true;
      callback.calledOnce.should.be.true;

      // tick
      net.tick();

      // the onReady callback should've been called exactly once
      callback.calledOnce.should.be.true;
    });

    it('onReady can be called many times - auto', function() {
      
      // create the net
      var net = jsn.Net({
        places: [
          { name: 'place', token: { counter: 0, ready: 0 } }
        ],

        transitions: [
          { name: 'test', onReady: 'test' },
          { name: 'count' }
        ],

        arcs: [
          { place: 'place', transition: 'test',
            evaluate: {
              ready: function(t) { return t.counter % 2 == 0; },
              update: {
                place: function(t) { return { ready: t.ready + 1 }; }
              }
            } },

          { place: 'place', transition: 'count',
            evaluate: {
              ready: true,
              update: {
                place: function(t) { return { counter: t.counter + 1 }; }
              }
            } }
        ]
      });

      // the onReady callback should've been called once
      var info = net.getPlace('place');
      info.token.should.have.property('ready', 1);
      
      // fire the counter
      net.fire('count');

      // the onReady callback should've been called once
      info = net.getPlace('place');
      info.token.should.have.property('ready', 1);
      
      // fire the counter
      net.fire('count');

      // the onReady callback should've been called twice
      info = net.getPlace('place');
      info.token.should.have.property('ready', 2);
    });

    it('onReady can be called many times', function() {
      
      // create the callback
      var callback = sinon.spy();

      // create the net
      var net = jsn.Net({
        places: [
          { name: 'place', token: { ready: true, value: 0 } }
        ],

        transitions: [
          { name: 'tested', onReady: callback },
          { name: 'make-ready' },
          { name: 'make-not-ready' }
        ],

        arcs: [
          { place: 'place', transition: 'tested',
            evaluate: {
              ready: function(t) { return t.ready === true; },
              update: {
                place: function(t) { return { value: t.value + 1 }; }
              }
            } },

          { place: 'place', transition: 'make-ready',
            evaluate: {
              ready: true,
              update: {
                place: function(t) { return { ready: true }; }
              }
            } },

          { place: 'place', transition: 'make-not-ready',
            evaluate: {
              ready: true,
              update: {
                place: function(t) { return { ready: false }; }
              }
            } }
            
        ]
      });

      // the onReady callback should've been called
      callback.calledOnce.should.be.true;

      // make the tested transition not ready
      net.fire('make-not-ready');

      // the onReady callback should've been called once
      callback.calledOnce.should.be.true;

      // make the tested transition ready again
      net.fire('make-ready');

      // the onReady callback should've been called twice
      (callback.callCount).should.eql(2);
    });

  }); // transition events
  

  describe.skip( 'automatic transitions', function() {
    
    it('fired on initialisation', function() {

      // create net
      var net = jsn.Net({
        places: [
          { name: 'machine', token: 1 },
          { name: 'store', token: 0 }
        ],

        transitions: [
          { name: 'produce', auto: true }
        ],

        arcs: [
          // produce, when raw materials are available
          { place: 'machine', transition: 'produce',
            evaluate: {
              ready: function(token) { return token > 0; },
              place: function(token) { return token - 1; }
            } },

          // the result of production
          { place: 'store', transition: 'produce',
            evaluate: {
              ready: true,
              place: function(token) { return token + 1; }
            } }
        ]
      });

      // check, whether the automatic transition has been fired

      // the transition shouldn't be ready
      var trans = net.readyTransitions();
      trans.length.should.eql(0);

      // the machine should be empty
      var place = net.getPlace('machine');
      place.should.have.property('token', 0);

      // the store should have one product
      place = net.getPlace('store');
      place.should.have.property('token', 1);

    });

    it('fired after other transitions', function() {
      
      // create net
      var net = jsn.Net({
        places: [
          { name: 'machine', token: 0 },
          { name: 'store', token: 0 }
        ],

        transitions: [
          { name: 'produce', auto: true },
          { name: 'buy' }
        ],

        arcs: [
          // buy a raw material
          { place: 'machine', transition: 'buy',
            evaluate: {
              ready: true,
              place: function(token) { return token + 1; }
            } },

          // produce, when raw materials are available
          { place: 'machine', transition: 'produce',
            evaluate: {
              ready: function(token) { return token > 0; },
              place: function(token) { return token - 1; }
            } },

          // the result of production
          { place: 'store', transition: 'produce',
            evaluate: {
              ready: true,
              place: function(token) { return token + 1; }
            } }
        ]
      });

      // fire the "buy" transition
      net.fire( 'buy' );

      // the machine should be empty
      var place = net.getPlace('machine');
      place.should.have.property('token', 0);

      // the store should have one product
      place = net.getPlace('store');
      place.should.have.property('token', 1);

    });

    it( 'fired after tick', function() {

      // create net
      var net = jsn.Net({
        places: [
          { name: 'machine', token: 0 },
          { name: 'store', token: 0 }
        ],

        transitions: [
          { name: 'produce', auto: true },
          { name: 'buy' }
        ],

        arcs: [
          // buy the raw material
          { place: 'machine', transition: 'buy',
            evaluate: {
              ready: true,
              place: function(t) { return t + 1; },
              time: 1
            } },

          // produce, when raw materials are available
          { place: 'machine', transition: 'produce',
            evaluate: {
              ready: function(t) { return t > 0; },
              place: function(t) { return t - 1; }
            } },

          // the result of production
          { place: 'store', transition: 'produce',
            evaluate: {
              ready: true,
              place: function(t) { return t + 1; }
            } }
        ]
      });

      // fire the buy transition
      net.fire( 'buy' );

      // the store should be empty
      var place = net.getPlace('store');
      place.should.have.property('token', 0);

      net.tick();

      // the machine should be empty
      place = net.getPlace('machine');
      place.should.have.property('token', 0);

      // the store should have one product
      place = net.getPlace('store');
      place.should.have.property('token', 1);
      
    });


  }); // automatic transitions

  it('evaluating arc expression does not affect tokens', function() {
    
    // create a net
    var net = jsn.Net({
      places: [
        { name: 'place', token: { test_place: 'ok' } }
      ],

      transitions: [
        { name: 'trans' }
      ],

      arcs: [
        { place: 'place', transition: 'trans',
          evaluate: {
            ready: function(token, global) {
              token.test_place = 'place Ooops';
              global.test_global = 'global Ooops';
              return false;
            },
            place: function(t) { return t; },
            global: function(g) { return g; }
          } }
      ],

      token: {
        test_global: 'ok'
      }
    });

    // evaluate the arc expression
    var info = net.readyTransitions();
    info.length.should.eql(0);

    // the tokens (both in the place, as well as the global one)
    // should be unchanged
    info = net.getPlace('place');
    info.should.have.property('token');
    info.token.should.have.property('test_place', 'ok');
    
    info = net.getToken();
    info.should.have.property('test_global', 'ok');
    
  });

  it('firing transition affects token only by the returned value', function() {
    
    var net = jsn.Net({
      places: [
        { name: 'place', token: { test: 'initial' } }
      ],

      transitions: [
        { name: 'act' }
      ],

      arcs: [
        { place: 'place', transition: 'act',
          evaluate: {
            ready: true,
            place: function(t) {
              t.new_test = 'Ooops';
              return {
                test: 'ok'
              };
            },
            global: function(g) {
              g.new_test = 'Ooops';
              return {
                test: 'ok'
              };
            }
          } }
      ],

      token: { test: 'initial' }
    });

    // fire the transition
    net.fire('act');

    // the tokens should have the values returned
    var info = net.getPlace('place');
    info.token.should.have.property('test', 'ok');
    info.should.not.have.property('new_test');
    
    info = net.getToken();
    info.should.have.property('test', 'ok');
    info.should.not.have.property('new_test');
    
  });
  
  describe( 'update token', function() {

    var spec;

    beforeEach( function() {
      spec = {
        places: [
          { name: 'place', token: { one_p: 1, test_p: 'Ooops' } }
        ],
    
        transitions: [
          { name: 'act' }
        ],
      
        arcs: [
          { place: 'place', transition: 'act',
            evaluate: {
              ready: true,
              update: {
                place: function() { return { two_p: 2, test_p: 'ok' }; },
                global: function() { return { two_g: 2, test_g: 'ok' }; }
              }
            } }
        ],
      
        token: {
          one_g: 1,
          test_g: 'Ooops'
        }
      };
    });

    it( 'update place', function() {

      // create a net
      var net = jsn.Net( spec );
      
      // fire the transition
      net.fire('act');
    
      // the place token should've been modified
      var info = net.getPlace('place');
      info.token.should.have.property('one_p', 1);
      info.token.should.have.property('two_p', 2);
      info.token.should.have.property('test_p', 'ok');
    
    });
    
    it( 'update global', function() {

      // create a net
      var net = jsn.Net( spec );

      // fire the transition
      net.fire('act');

      // the global token should've been modified
      var info = net.getToken();
      info.should.have.property('one_g', 1);
      info.should.have.property('two_g', 2);
      info.should.have.property('test_g', 'ok');
      
    });

    it( 'update with time', function() {

      // create a net
      spec.arcs[0].evaluate.time = 1;
      var net = jsn.Net( spec );

      // fire the transition
      net.fire('act');

      // the tokens should be unmodified
      var info = net.getPlace('place').token;
      info.should.have.property('one_p', 1);
      info.should.not.have.property('two_p');
      info.should.have.property('test_p', 'Ooops');
      var info = net.getToken();
      info.should.have.property('one_g', 1);
      info.should.not.have.property('two_g');
      info.should.have.property('test_g', 'Ooops');

      // tick
      net.tick();

      // the tokens should be modified
      info = net.getPlace('place').token;
      info.should.have.property('one_p', 1);
      info.should.have.property('two_p', 2);
      info.should.have.property('test_p', 'ok');
      info = net.getToken();
      info.should.have.property('one_g', 1);
      info.should.have.property('two_g', 2);
      info.should.have.property('test_g', 'ok');
      
    });

    it( 'update affects place and global only by the returned values', function() {
      
      // create a net
      spec.arcs[0].evaluate.update = {
        place: function(t) { 
          t.yada1 = 'Ooops';
          return { two_p: 2 };
        },
        global: function(g) { 
          //t.yada2 = 'Ooops'; 
          g.yada = 'Ooops';
          return { two_g: 2 };
        }
      };
      var net = jsn.Net( spec );

      // fire the transition
      net.fire('act');

      // the tokens should be modified
      var info = net.getPlace('place').token;
      info.should.not.have.property('yada1');
      info.should.not.have.property('yada2');
      info = net.getToken();
      info.should.not.have.property('yada');

    });

  });

  // this test is no longer valid
  it.skip('when there are no place nor global tokens, arc is not ready', function() {
    
    var arr = [];

    // create a net
    var net = jsn.Net({
      places: [
        { name: 'place' }
      ],

      transitions: [
        { name: 'act' }
      ],

      arcs: [
        { place: 'place', transition: 'act',
          evaluate: {
            ready: function() { arr.push(1); return true; },
            token: function() { return 1; }
          } }
      ]
    });

    // the transition shouldn't be ready
    var trans = net.readyTransitions();
    trans.length.should.eql(0);

    // the array should be empty, since the arc expression shouldn't be evaluated
    arr.length.should.eql(0);
  });

  
});
