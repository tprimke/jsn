'use strict';

var _slicedToArray = (function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i['return']) _i['return'](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError('Invalid attempt to destructure non-iterable instance'); } }; })();

var whatIs = function whatIs(val) {
  if (val === undefined) return 'undefined';

  if (val === null) return 'null';

  switch (typeof val) {
    case 'number':
      return 'number';

    case 'string':
      return 'string';

    case 'boolean':
      return 'boolean';

    case 'function':
      return 'function';

    default:
      if (val instanceof Array) return 'array';else return 'object';
  }
};

var clone = function clone(value) {
  switch (whatIs(value)) {
    case 'undefined':
      return undefined;

    case 'null':
      return null;

    case 'number':
    case 'boolean':
    case 'string':
    case 'function':
      return value;

    case 'array':
      return value.map(function (x) {
        return clone(x);
      });

    case 'object':
      var copy = {};
      for (var i in value) {
        if (value.hasOwnProperty(i)) copy[i] = clone(value[i]);
      }return copy;

    default:
      throw new Error('Unknown type');
  }
};

var intersection = function intersection(arr1, arr2) {
  return arr1.filter(function (elm) {
    return arr2.indexOf(elm) > -1;
  });
};

var uniq = function uniq(arr) {
  return arr.reduce(function (acc, e) {
    if (acc.indexOf(e) === -1) acc.push(e);
    return acc;
  }, []);
};

var any = function any(arr, pred) {
  return arr.reduce(function (acc, e) {
    if (acc === true) return true;
    return pred(e);
  }, false);
};

var all = function all(arr, pred) {
  return arr.reduce(function (acc, e) {
    if (acc === false) return false;
    return pred(e);
  }, true);
};

var find = function find(arr, pred) {
  return arr.filter(function (e) {
    return pred(e);
  })[0];
};

var min = function min(arr, name) {
  return arr.reduce(function (acc, elm) {
    return elm[name] < acc ? elm[name] : acc;
  }, arr[0]);
};

var partition = function partition(arr, pred) {
  return arr.reduce(function (acc, e) {
    var _acc = _slicedToArray(acc, 2);

    var arr_t = _acc[0];
    var arr_f = _acc[1];

    if (pred(e) === true) arr_t.push(e);else arr_f.push(e);
    return [arr_t, arr_f];
  }, [[], []]);
};

var merge = function merge(target, source) {
  for (var prop in source) {
    if (target.hasOwnProperty(prop)) {
      switch (whatIs(target[prop])) {
        case 'object':
          merge(target[prop], source[prop]);
          break;
        default:
          target[prop] = clone(source[prop]);
      }
    } else target[prop] = clone(source[prop]);
  }
};

// The main, object-creating function.
var Net = function Net() {
  var conf = arguments[0] === undefined ? {} : arguments[0];

  return (function () {

    // the similation time
    var time;

    // the global token
    // and the timed changes information for it
    // I'm not sure, whether it's needed at all.
    var token;
    var global_time;

    // the arrays of places, arcs and transitions
    // Initially, all of them are simply copies of their specification
    // objects.
    var places;
    var arcs;
    var transitions;

    // a placeholder for error message
    // The error handling really needs refactoring (and rethinking).
    // Right now, it's messed up.
    var error;

    // objects used to handle net events
    // events - the container for all events.
    //   events.place :: array
    //   events.transition :: array
    // event_id - the next event (unique) identifier to use
    // events_handled - the flag set to true, when the events are
    // being handled
    //
    // Probably all the events stuff should me moved to a separate
    // module.
    var events;
    var event_id;
    var events_handled;

    // --------------------------------------------------------------
    // private functions
    // --------------------------------------------------------------

    // returns a deep clone of the argument
    var copyThat = function copyThat(arr) {
      return clone(arr);
    };

    // This function, passed two arguments: the original value and the
    // default one, returns a copy (deep clone) of the original one
    // (if given), or the default one, when the original is not
    // specified.
    // It is used for initialisation of the net based on the given
    // specification.
    var getDefault = function getDefault(orig, def) {
      return orig !== undefined ? copyThat(orig) : def;
    };

    // This function should analyse the net.
    // Right now, it performs some basic checks (like unlinked places
    // or transitions).
    // It should be refactored and made more useful.
    var checkNet = function checkNet() {
      var places_of_arcs = arcs.map(function (arc) {
        return arc.place;
      });
      var names_of_places = places.map(function (p) {
        return p.name;
      });
      var common_places = intersection(places_of_arcs, names_of_places);
      // a place without an arc
      if (places.length > common_places.length) {
        error = 'There are disconnected places.';
        return;
      }

      // an arc of incorrect place
      places_of_arcs = uniq(places_of_arcs);
      names_of_places = uniq(names_of_places);
      common_places = intersection(places_of_arcs, names_of_places);
      if (places_of_arcs.length > common_places.length) {
        error = 'There are some missing places for arcs.';
        return;
      }

      // an arc without a place
      if (any(arcs, function (arc) {
        return !arc.hasOwnProperty('place');
      })) {
        error = 'There are arcs without a place.';
        return;
      }

      // an arc without a transition
      if (any(arcs, function (arc) {
        return !arc.hasOwnProperty('transition');
      })) {
        error = 'There are arcs without a transition.';
        return;
      }

      // an arc with incorrect evaluator
      if (any(arcs, function (arc) {
        if (whatIs(arc.evaluate) !== 'object') return true;
        if (!arc.evaluate.hasOwnProperty('ready')) return true;
        if (arc.evaluate.hasOwnProperty('time')) {
          if (whatIs(arc.evaluate.time) !== 'number') return true;
          if (Math.floor(arc.evaluate.time) !== arc.evaluate.time) return true;
          if (arc.evaluate.time < 1) return true;
        }
        if (arc.evaluate.hasOwnProperty('place')) return false;
        if (arc.evaluate.hasOwnProperty('global')) return false;
        if (arc.evaluate.hasOwnProperty('update')) {
          if (arc.evaluate.update.hasOwnProperty('place')) return false;
          if (arc.evaluate.update.hasOwnProperty('global')) return false;
          return true;
        }
        return true;
      })) {
        error = 'There are arcs with incorrect evaluators.';
        return;
      }

      var trans_of_arcs = arcs.map(function (a) {
        return a.transition;
      });
      var names_of_trans = transitions.map(function (t) {
        return t.name;
      });
      var common_trans = intersection(trans_of_arcs, names_of_trans);
      // a transition without an arc
      if (transitions.length > common_trans) {
        error = 'There are disconnected transitions.';
        return;
      }

      // an arc of incorrect transition
      trans_of_arcs = uniq(trans_of_arcs);
      names_of_trans = uniq(names_of_trans);
      common_trans = intersection(trans_of_arcs, names_of_trans);
      if (trans_of_arcs.length > common_trans.length) {
        error = 'There are some missing transitions for arcs.';
        return;
      }

      // checking doubled arcs
      var signatures = arcs.map(function (arc) {
        return arc.place + arc.transition;
      });
      var unique = uniq(signatures);
      if (unique.length < signatures.length) {
        error = 'There are some doubled arcs.';
        return;
      }
    };

    // Returns the array of all arcs bound with the transition of the
    // given name.
    var arcsOfTransition = function arcsOfTransition(t_name) {
      return arcs.filter(function (arc) {
        return arc.transition === t_name;
      });
    };

    // Returns the place objects, that the given arc object is bound
    // to.
    var placeOfArc = function placeOfArc(arc) {
      return find(places, function (place) {
        return place.name === arc.place;
      });
    };

    // Returns the value of the arc's ready evaluation expression.
    // The expression is either a value, or a function.
    // When the expression is a function, it is called with two
    // arguments: the arc's place token and the global token. Whatever
    // the function returns, is then returned by this function.
    var arcReady = function arcReady(arc) {
      var place = placeOfArc(arc);
      // seems to be incorrect
      // if ( (place.token === undefined) && (token === undefined) )
      //  return false;
      var value = undefined;
      try {
        if (whatIs(arc.evaluate.ready) === 'function') value = arc.evaluate.ready(clone(place.token), clone(token));else value = arc.evaluate.ready;
        return value;
      } catch (e) {
        error = e.toString();
        return false;
      }
    };

    // Evaluates the given arc object value.
    // This function is too complex, and it should be refactored.
    var arcValue = function arcValue(arc) {
      var place = placeOfArc(arc);
      var value = {};
      try {
        // timed values
        if (arc.evaluate.hasOwnProperty('time')) {
          value.time = arc.evaluate.time;

          // token changes
          if (arc.evaluate.hasOwnProperty('place')) value.place = arc.evaluate.place;
          if (arc.evaluate.hasOwnProperty('global')) value.global = arc.evaluate.global;

          // updates
          if (arc.evaluate.hasOwnProperty('update')) {
            value.update = {};
            if (arc.evaluate.update.hasOwnProperty('place')) value.update.place = arc.evaluate.update.place;
            if (arc.evaluate.update.hasOwnProperty('global')) value.update.global = arc.evaluate.update.global;
          }
        }

        // not-timed values
        else {
          // token changes
          if (arc.evaluate.hasOwnProperty('place')) value.place = arc.evaluate.place(clone(place.token), clone(token));
          if (arc.evaluate.hasOwnProperty('global')) value.global = arc.evaluate.global(clone(token));

          // updates
          if (arc.evaluate.hasOwnProperty('update')) {
            value.update = {};
            if (arc.evaluate.update.hasOwnProperty('place')) value.update.place = arc.evaluate.update.place(clone(place.token), clone(token));
            if (arc.evaluate.update.hasOwnProperty('global')) value.update.global = arc.evaluate.update.global(clone(token));
          }
        }

        return value;
      } catch (e) {
        error = e.toString();
        return false;
      }
    };

    // For the given transition object, returns true, iff the
    // transition is ready.
    var isReady = function isReady(t) {
      var the_arcs = arcsOfTransition(t.name);
      return all(the_arcs, function (arc) {
        return arcReady(arc);
      });
    };

    // Processes all the transitions "awaiting" for the "not-ready"
    // state. For each transition, that is not ready, the awaiting
    // state is changed to "ready".
    // This function is used to monitor transitions, which have the
    // onReady event.
    var checkAwaiting = function checkAwaiting() {
      var awaiting = transitions.filter(function (t) {
        return t.$awaiting === 'not-ready';
      }),
          not_ready = awaiting.filter(function (t) {
        return !isReady(t);
      });
      not_ready.forEach(function (t) {
        return t.$awaiting = 'ready';
      });
    };

    // Returns the next event identifier.
    var getEventId = function getEventId() {
      var id = event_id;
      event_id += 1;
      return id;
    };

    // Adds to the events queue the transition onTick event.
    //   handler.handler - the event handler, taken from the net
    //     specification
    //   handler.transition - the transition object
    //
    //   spec.type = 'ready'
    var queueTransitionOnReady = function queueTransitionOnReady(handler, spec) {
      events.transition.push({
        id: getEventId(),
        priority: 5,
        data: { handler: handler, spec: spec }
      });
    };

    // Adds to the events queue the transition onTick event.
    //   handler.handler - the event handler, taken from the net
    //     specification
    //   handler.transition - the transition object
    //
    //   spec.type = 'tick'
    var queueTransitionOnTick = function queueTransitionOnTick(handler, spec) {
      events.transition.push({
        id: getEventId(),
        priority: 1,
        data: { handler: handler, spec: spec }
      });
    };

    // Adds to the events queue the transition onFired event.
    //   handler.handler - the event handler, taken from the net
    //     specification
    //   handler.transition - the transition object
    //
    //   spec.type = 'fired'
    var queueTransitionOnFired = function queueTransitionOnFired(handler, spec) {
      events.transition.push({
        id: getEventId(),
        priority: 10,
        data: { handler: handler, spec: spec }
      });
    };

    // Adds to the events queue the place onUpdate event.
    //   handler.handler - the event handler, taken from the net
    //     specification
    //   handler.place - the place object
    //
    //   spec.type = 'update'
    var queuePlaceOnUpdate = function queuePlaceOnUpdate(handler, spec) {
      events.place.push({
        id: getEventId(),
        priority: 9,
        data: { handler: handler, spec: spec }
      });
    };

    // Adds a new event to the events queue.
    //   handler.handler: the handler for the event, taken from the
    //     net specification
    //   handler.place: the place object for the event
    //   handler.transition: the transition for the event
    //
    //   spec.type:
    //     'ready' for transition onReady
    //     'tick' for transition onTick
    //     'fired' for transition onFired
    //     'update' for place onUpdate
    var queueEvent = function queueEvent(handler, spec) {
      switch (spec.type) {
        case 'ready':
          queueTransitionOnReady(handler, spec);
          break;

        case 'tick':
          queueTransitionOnTick(handler, spec);
          break;

        case 'fired':
          queueTransitionOnFired(handler, spec);
          break;

        case 'update':
          queuePlaceOnUpdate(handler, spec);
          break;

        default:
          throw new Error('Unknown event type: ' + spec.type);
      }
    };

    var handlePlaceEvent = function handlePlaceEvent(info) {
      var spec = arguments[1] === undefined ? {} : arguments[1];

      // handler must be a function
      if (whatIs(info.handler) !== 'function') throw new Error('The onUpdate handler for the place ' + info.place.name + ' is not a function.');

      var token = getPlace(info.place.name).token;
      info.handler(token);
    };

    // Handles the given transition event.
    //   info.handler - the event handler
    //   info.transition - the transition object
    //   spec.processFired - ???
    //   spec.filter - ???
    var handleTransitionEvent = function handleTransitionEvent(info) {
      var spec = arguments[1] === undefined ? {} : arguments[1];

      if (spec.hasOwnProperty('processFired')) spec.processFired(info.transition);

      // handler is a function
      if (whatIs(info.handler) === 'function') {
        info.handler();
        return;
      }

      // handler is a transition name to fire
      var arr = [];
      if (whatIs(info.handler) === 'string') arr.push(info.handler);else arr = info.handler;

      // get the list of transitions
      arr = arr.map(function (name) {
        return find(transitions, function (t) {
          return t.name === name;
        });
      });

      // process the list, if needed
      if (spec.hasOwnProperty('filter')) arr = spec.filter(arr);

      // try to fire them
      arr.forEach(function (t) {
        if (isReady(t)) fire(t.name); // eslint-disable-line no-use-before-define
      });
    };

    // Handles a single event of the specified type:
    //   spec.type = 'transition', 'place'
    //   spec.handler = the function to handle event
    var handleEvent = function handleEvent(spec) {
      var evts = events[spec.type];

      // get the highest priority
      var priority = min(evts, 'priority').priority,

      // get the most important events from the queue
      the_events = evts.filter(function (e) {
        return e.priority === priority;
      });

      // remove the first event
      var evt = the_events.pop();
      events[spec.type] = evts.filter(function (e) {
        return e.id !== evt.id;
      });

      // handle the first event
      spec.handler(evt.data.handler, evt.data.spec);
    };

    // The main function for events handling.
    // Once invoked, cannot be invoked again, before the first one is
    // finished. (The `events_handled` variable is used for that
    // purpose.)
    // Handles all the events in the queue.
    var handleEvents = function handleEvents() {
      // don't let this function to be called many times
      if (events_handled === true) return;

      // set the flag to stop further calls to this function
      events_handled = true;

      // handle all the transition events
      while (events.transition.length > 0) handleEvent({
        type: 'transition',
        handler: handleTransitionEvent
      });

      // handle all the place events
      while (events.place.length > 0) handleEvent({
        type: 'place',
        handler: handlePlaceEvent
      });

      // set the flag to allow further calls to this function
      events_handled = false;
    };

    var checkTransitionEvent = function checkTransitionEvent(spec) {
      var events = spec.getEvents(); // eslint-disable-line no-shadow
      events.forEach(function (handler) {
        return queueEvent(handler, spec);
      });
    };

    var checkOnReady = function checkOnReady() {
      checkTransitionEvent({
        type: 'ready',
        getEvents: readyEvents,
        filter: function filter(arr) {
          return arr.filter(function (t) {
            return isReady(t);
          });
        },
        processFired: function processFired(t) {
          return t.$awaiting = 'not-ready';
        }
      });
    };

    var checkOnTick = function checkOnTick() {
      checkTransitionEvent({
        type: 'tick',
        getEvents: tickEvents
      });
    };

    // public functions
    var getTime = function getTime() {
      return time;
    };
    var tick = function tick() {
      time += 1;

      // update the global token
      var g_times = partition(global_time, function (t) {
        return t.time === time;
      });
      g_times[0].forEach(function (t) {
        if (t.hasOwnProperty('func')) token = t.func(clone(t.old_token));
        if (t.hasOwnProperty('update')) merge(token, t.update(clone(token)));
      });
      global_time = g_times[1];

      // update tokens in all places
      places.forEach(function (place) {
        if (!place.hasOwnProperty('time')) return;

        var times = partition(place.time, function (t) {
          return t.time === time;
        });

        // process all the times === time
        times[0].forEach(function (t) {
          var changed = false;
          if (t.hasOwnProperty('func')) {
            place.token = t.func(clone(t.old_token), clone(token));
            changed = true;
          }
          if (t.hasOwnProperty('update')) {
            merge(place.token, t.update(clone(place.token), clone(token)));
            changed = true;
          }
          if (changed) checkPlaceOnUpdate(place);
        });

        place.time = times[1];
      });

      checkOnReady();
      checkOnTick();

      handleEvents();
    };

    var getToken = function getToken() {
      return clone(token);
    };

    var readyTransitions = function readyTransitions() {
      var ready = transitions.filter(function (t) {
        return isReady(t);
      });
      return ready.map(function (t) {
        return t.name;
      });
    };

    var isTransReady = function isTransReady(name) {
      // find the transition
      var trans = transitions.filter(function (t) {
        return t.name === name;
      });

      // a transition, which doesn't exist, can't be ready (???)
      if (trans.length === 0) return false;

      return isReady(trans[0]);
    };

    // private
    var readyEvents = function readyEvents() {
      var ready = transitions.filter(function (t) {
        return t.$awaiting === 'ready' && isReady(t);
      });
      return ready.map(function (t) {
        return { handler: t.onReady, transition: t };
      });
    };

    var tickEvents = function tickEvents() {
      var trans = transitions.filter(function (t) {
        return t.onTick !== undefined;
      });
      return trans.map(function (t) {
        return { handler: t.onTick, transition: t };
      });
    };

    var checkPlaceOnUpdate = function checkPlaceOnUpdate(place) {
      if (place.hasOwnProperty('onUpdate')) queueEvent({ handler: place.onUpdate, place: place }, { type: 'update' });
    };

    var fire = function fire(name) {
      var trans = find(transitions, function (t) {
        return t.name === name;
      });
      if (trans === undefined) throw Error('No transition ' + name);
      if (!isReady(trans)) error = 'The transition ' + name + 'is not ready.';else {
        var the_arcs = arcsOfTransition(name);
        the_arcs.forEach(function (arc) {
          var value = arcValue(arc);
          var place = find(places, function (p) {
            return p.name === arc.place;
          });

          if (value.hasOwnProperty('time')) {
            var token_time = time + value.time;

            var place_info = {
              time: token_time
            };
            var global_info = {
              time: token_time
            };

            // timed token for place
            if (value.hasOwnProperty('place')) {
              if (!place.hasOwnProperty('time')) place.time = [];
              merge(place_info, {
                func: value.place,
                old_token: place.token
              });
              delete place['token'];
              checkPlaceOnUpdate(place);
            }

            // timed update for place
            if (value.hasOwnProperty('update') && value.update.hasOwnProperty('place')) {
              if (!place.hasOwnProperty('time')) place.time = [];
              place_info.update = value.update.place;
            }

            if (place_info.hasOwnProperty('func') || place_info.hasOwnProperty('update')) place.time.push(place_info);

            // timed global token
            if (value.hasOwnProperty('global')) {
              merge(global_info, {
                func: value.global,
                old_token: token
              });
              token = undefined;
            }

            // times update for the global token
            if (value.hasOwnProperty('update') && value.update.hasOwnProperty('global')) {
              global_info.update = value.update.global;
            }

            if (global_info.hasOwnProperty('func') || global_info.hasOwnProperty('update')) global_time.push(global_info);
          } else {
            if (value.hasOwnProperty('place')) {
              place.token = value.place;
              checkPlaceOnUpdate(place);
            }

            if (value.hasOwnProperty('global')) token = value.global;

            if (value.hasOwnProperty('update')) {
              if (value.update.hasOwnProperty('place')) {
                merge(place.token, value.update.place);
                checkPlaceOnUpdate(place);
              }
              if (value.update.hasOwnProperty('global')) merge(token, value.update.global);
            }
          }
        });

        checkAwaiting();

        checkOnReady();

        if (trans.hasOwnProperty('onFired')) {
          queueEvent({ handler: trans.onFired, transition: trans }, { type: 'fired' });
        }

        handleEvents();
      }
    };

    var getPlaces = function getPlaces() {
      return places.map(function (place) {
        return clone(place);
      });
    };

    var getPlace = function getPlace(name) {
      var place = find(places, function (p) {
        return p.name === name;
      });
      if (place !== undefined) return clone(place);else throw Error('No place ' + name);
    };

    var getError = function getError() {
      return error;
    };

    var reset = function reset() {
      return initNet();
    };

    //private
    var initNet = function initNet() {
      places = getDefault(conf.places, []);
      token = getDefault(conf.token, undefined);
      arcs = getDefault(conf.arcs, []);
      transitions = getDefault(conf.transitions, []);
      time = 0;
      global_time = [];
      events = {
        transition: [],
        place: []
      };
      event_id = 0;
      events_handled = false;

      // set the $awaiting for transitions
      var on_ready_trans = transitions.filter(function (t) {
        return t.hasOwnProperty('onReady');
      });
      on_ready_trans.forEach(function (t) {
        return t.$awaiting = 'ready';
      });

      //fireAutoTransitions();
      checkOnReady();

      handleEvents();
    };

    // the initialisation

    initNet();
    checkNet();

    return {
      getTime: getTime,
      tick: tick,
      getToken: getToken,
      readyTransitions: readyTransitions,
      isReady: isTransReady,
      fire: fire,
      getPlaces: getPlaces,
      getPlace: getPlace,
      getError: getError,
      reset: reset
    };
  })();
};

exports.Net = Net;