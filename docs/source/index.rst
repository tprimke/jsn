.. JSN documentation master file, created by
   sphinx-quickstart on Fri Jul  3 14:01:13 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to JSN's documentation!
===============================

Contents:

.. toctree::
   :titlesonly:

   testing
   tutorial


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

