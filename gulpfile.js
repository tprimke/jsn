var gulp       = require('gulp'),
    to5        = require('gulp-babel'),
    del        = require('del'),
    vinylPaths = require('vinyl-paths'),
    rename     = require('gulp-rename'),
    mocha      = require('gulp-mocha');

// Build JSN

gulp.task( 'clean', function() {
  return gulp.src( 'build/*' )
             .pipe( vinylPaths(del) );
});

gulp.task( 'build-es6', ['clean'], function() {
  return gulp.src('src/**/*.es6')
             .pipe(to5())
             .pipe(rename( function(path) {
               path.extname = '.js';
             }))
             .pipe( gulp.dest('build') );
});

gulp.task( 'build', ['build-es6'], function() {
  return gulp.src('build/jsn.js')
             .pipe( gulp.dest('dist') );
});

// Test

gulp.task( 'test', ['build-es6'], function() {
  return gulp.src(['test/test-*.js'], {read: false})
             .pipe(mocha({
               reporter: 'spec'
             }));
});

